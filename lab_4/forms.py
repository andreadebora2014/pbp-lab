from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'fromNote', 'title', 'message']
    
    to = forms.CharField(label='Reciever', max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Received by'}))
    fromNote = forms.CharField(label='Sender', max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Send by'}))
    title = forms.CharField(label='Title', max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'The title'}))
    message = forms.CharField(label='Message', max_length=120, widget=forms.TextInput(attrs={'placeholder' : 'The message'}))