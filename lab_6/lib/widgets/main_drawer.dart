import 'package:flutter/material.dart';

//import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'Segoe UI',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      //onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color(0xff343434),
            child: Text(
              'Kladaf',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Color(0xFFe0e1dd)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Diary', Icons.border_color, () {
            //Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Podomoro Timer', Icons.timer, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('Weekly Schedule', Icons.date_range, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('Progress Tracker', Icons.inventory, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('Wish List', Icons.fact_check, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
        ],
      ),
    );
  }
}
