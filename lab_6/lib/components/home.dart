import 'package:flutter/material.dart';
import 'package:lab_6/widgets/main_drawer.dart';
import 'package:lab_6/components/body.dart';

class HomeLab6 extends StatelessWidget {
  const HomeLab6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Weekly Schedule',
          style: TextStyle(
            color: Color(0xFFe0e1dd),
            fontFamily: 'Segoe UI',
          ),
        ),
        backgroundColor: Color(0xff343434),
      ),
      drawer: MainDrawer(),
      body: MainBody(),
    );
  }
}
