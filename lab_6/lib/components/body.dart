import 'package:flutter/material.dart';

class MainBody extends StatelessWidget {
  TextEditingController activityController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController detailController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  bool monVal = false;
  bool tuVal = false;
  bool wedVal = false;
  bool thursVal = false;
  bool friVal = false;
  bool satVal = false;
  bool sunVal = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(0xff0d1b2a),
        child: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(children: <Widget>[
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Add Weekly Schedule',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontWeight: FontWeight.w500,
                        fontSize: 30),
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                  child: Text(
                    'Fill The Information',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontSize: 20),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: activityController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Activity here',
                        hintText: ' Ex: Study web development lecture',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: timeController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Time here',
                        hintText: ' Ex: 06.00',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: detailController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Detail here',
                        hintText:
                            ' Ex: Try to solve 25 problems in 1 hour without looking on the internet',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: messageController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Message here',
                        hintText: ' Ex: Always pray before study',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Theme(
                data: ThemeData(unselectedWidgetColor: Color(0xFF6096ba)),
                child: Container(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Mon",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              value: monVal,
                              onChanged: (bool? value) {
                                monVal = value!;
                              },
                            ),
                          ],
                        ),
                        // [Tuesday] checkbox
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Tu",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              checkColor: Colors.white,
                              value: tuVal,
                              onChanged: (bool? value) {
                                tuVal = value!;
                              },
                            ),
                          ],
                        ),
                        // [Wednesday] checkbox
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Wed",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              activeColor: Colors.white,
                              value: wedVal,
                              onChanged: (bool? value) {
                                wedVal = value!;
                              },
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Thur",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              //unselected: Colors.white,
                              value: wedVal,
                              onChanged: (bool? value) {
                                wedVal = value!;
                              },
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Fri",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              checkColor: Colors.white,
                              value: wedVal,
                              onChanged: (bool? value) {
                                wedVal = value!;
                              },
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Sat",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              checkColor: Colors.white,
                              value: wedVal,
                              onChanged: (bool? value) {
                                wedVal = value!;
                              },
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Sun",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                            Checkbox(
                              checkColor: Colors.white,
                              value: wedVal,
                              onChanged: (bool? value) {
                                wedVal = value!;
                              },
                            ),
                          ],
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Center(
                    child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                    primary: Color(0xFF415a77),
                  ),
                  child: Text('Add Schedule'),
                  onPressed: () {
                    print(activityController.text);
                    print(timeController.text);
                    print(detailController.text);
                    print(messageController.text);
                  },
                )),
              ),
            ])));
  }
}
