1. Apakah perbedaan antara JSON dan XML?
Berikut merupakan beberapa perbedaan antara JSON dan XML :
- JSON merupakan singkatan dari JavaScript Object Notation dan didasarkan oleh bahasa pemrograman JavaScript. Sementara itu, XML merupakan singkatan dari Extensible Markup Language yang diturunkan dari Standard Generalized
Markup Language (SGML).
- JSON berguna ketika ingin mengirimkan data secara cepat karena proses penguraian datanya cepat. Sementara itu, XML berguna ketika ingin mengirimkan data secara lambat karenna proses penguraian datanya lambat.
- JSON merepresentasikan objects. Sementara itu, XML merepresentasikan data yang ditampilkan dalam bentuk tag structure.
- JSON memiliki sintaks yang lebih mudah untuk dibaca sehingga kurang aman. Sementara itu, sintaks XML relatif sulit untuk dibaca karena mirip dengan sejumlah markup lain sehingga jauh lebih aman.
- JSON mendukung beragam jenis tipe data yang kompleks (seperti diagram, gambar, dan data non-primitif lain) namun tidak mendukung penggunaan array. Sementara itu, XML hanya mendukung string, angka, array Boolean, dan objek primitif namun mendukung penggunaan array.
- JSON mendukung encoding UTF dan ASCII. Sementara itu, XML mendukung encoding UTF-8 dan UTF-16
- JSON lebih mudah dan cepat digunakan untuk AJAX. Sementara itu, XML lebih sulit dan lambat digunakan untuk AJAX.

Referensi :
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://hackr.io/blog/json-vs-xml


2. Apakah perbedaan antara HTML dan XML?
Perbedaan antara HTML dan XML adalah :
- HTML merupakan singkatan dari HyperText Markup Language. Sementara itu, XML merupakan singkatan dari Extensible Markup Language.
- HTML merupakan markup language. Sementara itu, XML menyediakan framework untuk mendefinisikan markup language.
- HTML digunakan untuk menampilkan data (tanpa membawa data tersebut). Sementara itu, XML digunakan untuk menyimpan data dan membawanya dari dan/atau ke database.
- HTML bersifat statis. Sementara itu, XML bersifat dinamis.
- HTML tidak bersifat case-sensitive dan mentoleransi error kecil. Sementara itu, XML bersifat case-sensitive dan tidak memperbolehkan ada error.
- HTML memiliki tags yang terbatas dan telah ditentukan dari awal. Sementara itu, XML memiliki tags yang dapat ditambahkan dan ditentukan oleh pengguna.

Referensi:
https://www.geeksforgeeks.org/html-vs-xml/