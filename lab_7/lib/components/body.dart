import 'package:flutter/material.dart';

class MainBody extends StatelessWidget {
  TextEditingController activityController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController detailController = TextEditingController();
  TextEditingController messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(0xff0d1b2a),
        child: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(children: <Widget>[
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Add Weekly Schedule',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontWeight: FontWeight.w500,
                        fontSize: 30),
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                  child: Text(
                    'Fill The Information',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontSize: 20),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: activityController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Activity here',
                        hintText: ' Ex: Study web development lecture',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: timeController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Time here',
                        hintText: ' Ex: 06.00',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: detailController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Detail here',
                        hintText:
                            ' Ex: Try to solve 25 problems in 1 hour without looking on the internet',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: messageController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type your Message here',
                        hintText: ' Ex: Always pray before study',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Theme(
                data: ThemeData(unselectedWidgetColor: Color(0xFF6096ba)),
                child: Container(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // [Monday] checkbox
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Mon",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                        // [Tuesday] checkbox
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Tue",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                        // [Wednesday] checkbox
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Wed",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Thur",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Fri",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Sat",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyStatefulWidget(),
                            Text(
                              "Sun",
                              style: TextStyle(color: Color(0xFFd9d9d9)),
                            ),
                          ],
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Center(
                    child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                    primary: Color(0xFF415a77),
                  ),
                  child: Text('Add Schedule'),
                  onPressed: () {
                    print(activityController.text);
                    print(timeController.text);
                    print(detailController.text);
                    print(messageController.text);
                    //print(_MyStatefulWidgetState().isChecked);
                  },
                )),
              ),
            ])));
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Color(0xFF6096ba);
    }

    return Checkbox(
      checkColor: Color(0xFFd9d9d9),
      fillColor: MaterialStateProperty.resolveWith(getColor),
      value: isChecked,
      onChanged: (bool? value) {
        setState(() {
          isChecked = value!;
        });
      },
    );
  }
}
