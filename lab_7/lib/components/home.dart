import 'package:flutter/material.dart';
import 'package:lab_7/widgets/main_drawer.dart';
import 'package:lab_7/components/body.dart';

class HomeLab7 extends StatelessWidget {
  const HomeLab7({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Weekly Schedule',
          style: TextStyle(
            color: Color(0xFFe0e1dd),
            fontFamily: 'Segoe UI',
          ),
        ),
        backgroundColor: Color(0xff343434),
      ),
      drawer: MainDrawer(),
      body: MainBody(),
    );
  }
}
